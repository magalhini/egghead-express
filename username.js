var express = require('express');
var helpers = require('./helpers');
var fs = require('fs');

var User = require('./db/database').User;

var router = express.Router({
    mergeParams: true
});

router.use(function(req, res, next) {
    console.log(req.method, 'for', req.params.username, 'at', req.path);
    next();
});

router.get('/', function(req, res) {
    var username = req.params.username;
    User.findOne({username: username}, function(err, user) {
        res.render('user', {
            user: user,
            address: user.location
        });
    });
});

router.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

router.get('/edit', function (req, res) {
    res.send('You want to edit ' + req.params.username + '???');
});

router.put('/', function(req, res) {
    var username = req.params.username;

    User.findOneAndUpdate({username: username}, {
        location: req.body
    }, function(err, user) {
        res.end();
    });

});

module.exports = router;

var uri = 'mongodb://localhost:27017/eggxpress';
var mongoose = require('mongoose');
var _ = require('lodash');

mongoose.connect(uri);

var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function() { console.log('DB connected'); });

var userSchema = mongoose.Schema({
    username: String,
    gender: String,
    name: {
        title: String,
        first: String,
        last: String
    },
    location: {
        street: String,
        city: String,
        state: String,
        zip: Number
    }
});

// Re-create the missing name: {full : ...} from the schema
userSchema.virtual('name.full').get(function() {
    return _.startCase(this.name.first + ' ' + this.name.last);
});

exports.User = mongoose.model('User', userSchema);

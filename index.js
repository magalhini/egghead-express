var express = require('express');
var app = express();

var fs = require('fs');
var _ = require('lodash');
var engines = require('consolidate');
var bodyParser = require('body-parser');
var path = require('path');
var JSONStream = require('JSONStream');
var users = [];

var helpers = require('./helpers');

var User = require('./db/database').User;

app.engine('hbs', engines.handlebars);

app.set('views', './views');
app.set('view engine', 'hbs');
app.use(express.static('images'));
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', function (req, res) {
    /*var users = [];
    fs.readdir('users', function (err, files) {
        files.forEach(function (file) {
            if (!file.indexOf('DS_') > -1) {
                fs.readFile(path.join(__dirname, 'users', file), {encoding: 'utf8'}, function (err, data) {
                    var user = JSON.parse(data);
                    user.name.full = _.startCase(user.name.first + ' ' + user.name.last);
                    users.push(user);
                    if (users.length === files.length) res.render('index', {users: users});
                });
            }
        });
    });*/

    User.find({}, function(err, users) {
        res.render('index', { users: users });
    });
});

app.get('*.json', function(req, res) {
    res.download('./users/' + req.path);
});

app.get('/data/:username', function(req, res) {
    var username = req.params.username;
    var user = helpers.getUser(username);
    res.json(user);
});

var userRouter = require('./username');
app.use('/:username', userRouter);

app.get('/error/:username', function(req, res) {
    res.status(404).send('No user found for ' + req.params.username);
});

app.get('/users/by/:gender', function(req, res) {
    var gender = req.params.gender;
    var readable = fs.createReadStream('users.json');

    User.find({gender: gender}, function(err, users) {

        if (users.length) {
            users = users.map(function(user) {
                return user.username;
            });
        } else {
            res.send('No users');
        }

        res.send(users);
    });

    /*readable.pipe(JSONStream.parse('*', function(user) {
        if (user.gender === gender) return user;
    }))
        .pipe(JSONStream.stringify('[\n  ', ',\n  ', '\n], \n'))
        .pipe(res);*/
});

var server = app.listen(8000, function() {
    console.log('Server at port', server.address().port);
});
